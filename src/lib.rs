use std::{fmt, io};

pub mod cache;
pub mod config;
pub mod console_style;
pub mod docker;
mod gitlab;
pub mod layers;
pub mod monitor;
pub mod package;
pub mod pipeline;

trait ErrorConverter<T, E>
where
    E: fmt::Display,
{
    fn with_context<S: AsRef<str>>(self, context: S) -> Result<T, String>;
}

impl<T, E> ErrorConverter<T, E> for Result<T, E>
where
    E: fmt::Display,
{
    fn with_context<S: AsRef<str>>(self, context: S) -> Result<T, String> {
        self.map_err(|e| format!("{}: {}", context.as_ref(), e))
    }
}

#[derive(Debug)]
pub enum CliError {
    ExitCode(i32),
    Message(String),
}

impl From<String> for CliError {
    fn from(s: String) -> CliError {
        CliError::Message(s)
    }
}

impl From<i32> for CliError {
    fn from(code: i32) -> CliError {
        CliError::ExitCode(code)
    }
}

impl From<io::Error> for CliError {
    fn from(e: io::Error) -> Self {
        CliError::Message(format!("{}", e))
    }
}

pub type CliResult = Result<(), CliError>;
