use std::{
    collections::HashMap,
    fs,
    io::{self, ErrorKind},
    path::{Path, PathBuf},
};

use clap::{Args, Subcommand};

use cmd_lib::{run_cmd, run_fun};

use log::{debug, info, trace, warn};
use serde::{Deserialize, Serialize};
use serde_yaml;

use crate::{CliError, CliResult, ErrorConverter};

#[derive(Debug, Args)]
#[clap(about = "set up & interact with yocto layers")]
pub struct LayerCommand {
    #[clap(subcommand)]
    command: LayerSubCommand,
    #[clap(long, short = 'd', global = true, default_value = ".")]
    layer_dir: String,
}

#[derive(Debug, Subcommand)]
#[clap(about = "set up & interact with yocto layers")]
pub enum LayerSubCommand {
    Fetch(FetchArgs),
    Digest(DigestArgs),
}

#[derive(Debug, Args)]
pub struct DigestArgs {
    #[clap(long, default_value = "")]
    target: String,
    #[clap(long, short = 'b')]
    base_config: Option<String>,
}

#[derive(Debug, Args)]
pub struct FetchArgs {
    #[clap(short, long = "config")]
    config_path: String,
    #[clap(long)]
    default_override: Option<String>,
    #[clap(value_parser=parse_overrides)]
    #[clap(long)]
    overrides: Option<HashMap<String, String>>,
}

fn parse_overrides(s: &str) -> Result<HashMap<String, String>, String> {
    let mut map = HashMap::new();
    for line in s.split(',') {
        let mut fields = line.split(':');
        let layer = fields.next();
        let layerref = fields.next();
        if layer.is_none() || layerref.is_none() {
            return Err(format!("failed to parse override: \"{}\"", line));
        }
        map.insert(
            String::from(layer.unwrap()),
            String::from(layerref.unwrap()),
        );
    }
    Ok(map)
}
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Layer {
    pub name: String,
    pub url: String,
    #[serde(default = "Layer::default_ref")]
    #[serde(rename = "ref")]
    pub reference: String,
    #[serde(default)]
    pub pinned: bool,
    #[serde(default)]
    pub layers: Vec<String>,
}

impl Layer {
    fn default_ref() -> String {
        String::from("main")
    }

    fn with_ref(&self, newref: String) -> Layer {
        let mut newlayer = self.clone();
        newlayer.reference = newref;
        newlayer
    }

    fn with_layers(self, layers: Vec<String>) -> Self {
        Self { layers, ..self }
    }
}

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct Config {
    pub local: HashMap<String, String>,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Build {
    pub target: String,
    pub sources: Vec<Layer>,
    #[serde(default)]
    pub conf: Config,
}

impl Build {
    pub fn read_from_disk(config_file_path: &Path) -> Result<Build, CliError> {
        let text =
            fs::read_to_string(config_file_path).with_context("failed to read build config")?;

        let config = serde_yaml::from_str(&text).with_context("failed to parse build config")?;
        Ok(config)
    }

    pub fn with_sources(self, sources: Vec<Layer>) -> Self {
        Self { sources, ..self }
    }
}

pub fn process_command(command: LayerCommand) -> CliResult {
    match &command.command {
        LayerSubCommand::Fetch(opts) => process_fetch(&command, opts),
        LayerSubCommand::Digest(args) => process_digest(&command, args),
    }
}

fn process_fetch(command: &LayerCommand, opts: &FetchArgs) -> CliResult {
    let config = Build::read_from_disk(Path::new(&opts.config_path))?;
    debug!("config target: {}", config.target);

    let layers = apply_overrides(&opts.default_override, &opts.overrides, &config);
    let path = match fs::canonicalize(&command.layer_dir) {
        Ok(p) => Ok(p),
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            fs::create_dir_all(&command.layer_dir)
                .with_context("unable to create layer directory")?;
            fs::canonicalize(&command.layer_dir)
                .with_context("unable to get path to layer directory")
        }
        Err(e) => Err(format!("{}", e)),
    }?;

    for layer in layers {
        pull_layer(&path, &layer)?;
    }
    Ok(())
}

fn get_subpath<'a>(possible_child: &'a Path, possible_parent: &Path) -> Option<&'a Path> {
    if possible_parent == possible_child {
        // strip_prefix returns an empty path for exact matches, but we want `None` instead
        // to avoid generating a list of empty subpaths
        None
    } else {
        possible_child.strip_prefix(possible_parent).ok()
    }
}

fn read_bitbake_layers() -> Result<Option<Vec<PathBuf>>, String> {
    let output = match run_fun!(bitbake-layers show-layers) {
        Ok(output) => output,
        Err(e) => {
            warn!("could not run bitbake-layers: {e}, only listing repositories");
            return Ok(None);
        }
    };
    let lines = output.lines();
    let lines = lines.skip_while(|&line| !line.starts_with('=')).skip(1);
    let layers = lines
        .map(|line| {
            let mut split = line.split_ascii_whitespace();
            let name = split.next().unwrap();
            let path = PathBuf::from(split.next().unwrap());
            let path = path
                .canonicalize()
                .with_context(format!("failed to canonicalize path for {name}"))?;
            let priority = split.next().unwrap();
            debug!(
                "found layer {name} at {} with priority {priority}",
                path.display()
            );
            Ok(path)
        })
        .collect::<Result<Vec<_>, String>>()?;
    Ok(Some(layers))
}

fn find_subpaths(repository: &Path, layers: &[PathBuf]) -> Vec<String> {
    layers
        .iter()
        .filter_map(|l| get_subpath(l, repository))
        .map(|l| l.to_string_lossy().into())
        .collect()
}

fn read_layers_from_layer_dir(layer_dir: &Path) -> Result<Vec<(Layer, PathBuf)>, CliError> {
    let layer_dir_contents = fs::read_dir(layer_dir).with_context("failed to read layer dir")?;
    let dirs = layer_dir_contents.filter(|entry| match entry {
        Ok(d) => {
            let path = d.path();
            path.is_dir()
        }
        Err(_) => false,
    });

    let mut layers = Vec::new();
    for dir in dirs {
        let path = dir.unwrap().path();
        let path = path
            .canonicalize()
            .with_context(format!("failed to canonicalize path {}", path.display()))?;
        let layer = read_layer_info(&path)?;
        layers.push((layer, path));
    }
    Ok(layers)
}

fn read_layer_info(layer_path: &Path) -> Result<Layer, CliError> {
    let remotes = run_fun!(git -C ${layer_path} remote).with_context("failed to get remotes")?;
    debug!("found remotes: {remotes}");
    let first_remote = remotes
        .lines()
        .next()
        .ok_or(CliError::Message("failed to get first remote".into()))?;
    let url = run_fun!(git -C ${layer_path} remote get-url ${first_remote})
        .with_context("failed to get remote URL")?;
    let revision =
        run_fun!(git -C ${layer_path} rev-parse HEAD).with_context("failed to find revision")?;
    let layer = Layer {
        name: layer_path.file_name().unwrap().to_str().unwrap().into(),
        url,
        reference: revision,
        pinned: false, // we have no way to detect whether the layer was originally pinned or not
        layers: Vec::new(),
    };
    Ok(layer)
}

fn read_base_config(base_config_path: Option<&Path>) -> Result<Option<Build>, CliError> {
    if let Some(base_config_path) = base_config_path {
        let build = Build::read_from_disk(base_config_path)?;
        Ok(Some(build))
    } else {
        Ok(None)
    }
}

fn process_digest(command: &LayerCommand, args: &DigestArgs) -> CliResult {
    let found_layers = read_layers_from_layer_dir(Path::new(&command.layer_dir))?;
    let bitbake_layers = read_bitbake_layers()?;
    let base_config = read_base_config(args.base_config.as_ref().map(Path::new))?;

    let found_layers = found_layers
        .into_iter()
        .map(|(layer, layer_path)| {
            let subpaths = if let Some(bitbake_layers) = bitbake_layers.as_ref() {
                find_subpaths(&layer_path, bitbake_layers)
            } else if let Some(base_config) = base_config.as_ref() {
                base_config
                    .sources
                    .iter()
                    .find(|&s| s.name == layer.name)
                    .map(|l| l.layers.clone())
                    .unwrap_or_default()
            } else {
                Vec::new()
            };

            layer.with_layers(subpaths)
        })
        .collect();

    let build = base_config.unwrap_or_default().with_sources(found_layers);

    let yaml = serde_yaml::to_string(&build).with_context("failed to serialize build config")?;
    println!("{}", yaml);
    Ok(())
}

fn apply_overrides(
    default_override: &Option<String>,
    overrides: &Option<HashMap<String, String>>,
    config: &Build,
) -> Vec<Layer> {
    let overrides = overrides.clone().unwrap_or_default();
    let apply_override = |overrides: &HashMap<String, String>, layer: &Layer| {
        if layer.pinned {
            warn!(
                "layer {} is pinned to {}, ignoring all overrides",
                layer.name, layer.reference
            );
            layer.clone()
        } else {
            overrides
                .get(&layer.name)
                .map(|newref| layer.with_ref(newref.clone()))
                .or_else(|| {
                    default_override
                        .clone()
                        .map(|newref| layer.with_ref(newref))
                })
                .unwrap_or_else(|| layer.clone())
        }
    };

    config
        .sources
        .iter()
        .map(|layer| apply_override(&overrides, layer))
        .collect()
}

fn pull_layer(base_dir: &Path, layer: &Layer) -> CliResult {
    let mut layer_dir = base_dir.to_path_buf();
    layer_dir.push(layer.name.clone());
    match fs::canonicalize(&layer_dir) {
        Ok(path) => update_layer(&path, layer),
        Err(err) if err.kind() == ErrorKind::NotFound => clone_layer(base_dir, layer),
        Err(err) => Err(format!("{}", err).into()),
    }
}

fn clone_layer(target_dir: &Path, layer: &Layer) -> CliResult {
    info!("cloning layer {}", layer.url);
    let layer_path = target_dir.to_str().unwrap();
    let layer_name = &layer.name;
    let layer_ref = layer.reference.clone();
    let url = layer.url.clone();
    run_cmd!(git -C ${layer_path} clone ${url} ${layer_name}).with_context("git clone failed")?;
    run_cmd!(git -C ${layer_path}/${layer_name} checkout ${layer_ref})
        .with_context("git clone failed")?;
    Ok(())
}

fn update_layer(layer_dir: &Path, layer: &Layer) -> CliResult {
    let layer_path = layer_dir.to_str().unwrap();
    let layer_ref = layer.reference.clone();
    info!("updating layer {}", layer.name);
    let local_branches = run_fun!(git -C ${layer_path} branch --format="%(refname:lstrip=-1)" -a)
        .with_context("failed to get local branches")?;
    trace!("found branches: {local_branches:?}");
    let is_branch = local_branches.lines().any(|branch| branch == layer_ref);
    if is_branch {
        debug!("target ref {layer_ref} is a branch");
        run_cmd!(git -C ${layer_path} checkout ${layer_ref}).with_context("git checkout failed")?;
        run_cmd!(git -C ${layer_path} pull).with_context("git pull failed")?;
    } else {
        debug!("target ref {layer_ref} is not a branch");
        run_cmd!(git -C ${layer_path} fetch).with_context("git fetch failed")?;
        run_cmd!(git -C ${layer_path} checkout ${layer_ref}).with_context("git checkout failed")?;
    }

    Ok(())
}
