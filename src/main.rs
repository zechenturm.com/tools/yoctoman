use std::process;

use clap::{Args, Parser, Subcommand};

use env_logger::Env;
use log::{debug, error, info, trace, warn};
use yoctoman::cache::{self, CacheCommand};
use yoctoman::config::{self, ConfigArgs};
use yoctoman::layers::LayerCommand;
use yoctoman::monitor;
use yoctoman::monitor::MonitorCommand;
use yoctoman::package::PackageCommand;
use yoctoman::pipeline;
use yoctoman::pipeline::PipelineCommand;
use yoctoman::{docker, layers};
use yoctoman::{package, CliError};

/// yoctoman - a collection of commands to manage yocto builds and their results, use `help` to see full help
///
/// Each yoctoman subcommand manages a different aspect of my yocto builds, both locally and in CI,
/// including managing Gitlab pipelines and packages to publish the resulting images et. al.
///
/// To change log verbosity, set `RUST_LOG` to the appropriate level (trace, debug, info, warn, error),
/// defaults to `info` if not present
/// For more info see [https://docs.rs/env_logger]
#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(propagate_version = true, verbatim_doc_comment)]
struct Cli {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    Layers(LayerCommand),
    #[clap(subcommand)]
    Pipeline(PipelineCommand),
    #[clap(subcommand)]
    Package(PackageCommand),
    /// test logger output
    Debug,
    Monitor(MonitorCommand),
    #[clap(subcommand)]
    Cache(CacheCommand),
    /// use and manage docker based build environments (not functional yet)
    Docker,
    Config(ConfigArgs),
    Version,
}

#[derive(Debug, Args)]
struct BuildArguments {
    #[clap(short, long)]
    machine: String,
    #[clap(short, long)]
    build_type: String,
}

#[tokio::main]
async fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
    let cli: Cli = Parser::parse();

    let res = match cli.command {
        Command::Layers(cmd) => layers::process_command(cmd),
        Command::Pipeline(cmd) => pipeline::process_pipeline_command(cmd),
        Command::Package(cmd) => package::execute(cmd),
        Command::Monitor(cmd) => monitor::process(cmd),
        Command::Cache(cmd) => cache::process(cmd),
        Command::Docker => docker::process().await,
        Command::Config(args) => config::process(args),
        Command::Debug => {
            trace!("{:?}", cli);
            debug!("{:?}", cli);
            info!("{:?}", cli);
            warn!("{:?}", cli);
            error!("{:?}", cli);
            Ok(())
        }
        Command::Version => {
            // these variables are set by Gitlab in its CI pipelines
            let version = option_env!("CI_COMMIT_SHA").unwrap_or("unknown");
            let timestamp = option_env!("CI_COMMIT_TIMESTAMP").unwrap_or("unknown");
            let title = option_env!("CI_COMMIT_TITLE").unwrap_or("unknown");

            info!("yoctoman was built from commit {version} which was committed on {timestamp}: \"{title}\"");
            Ok(())
        }
    };
    match res {
        Ok(()) => {}
        Err(err) => match err {
            CliError::ExitCode(code) => process::exit(code),
            CliError::Message(msg) => {
                error!("{}", msg);
                process::exit(128);
            }
        },
    }
}
