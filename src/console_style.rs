pub enum Color {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
}

pub enum FB {
    Foreground(Color),
    Background(Color),
}

pub enum Style {
    Reset,
    Regular(FB),
    Bright(FB),
}

pub fn set_console(style: Style) -> String {
    let mut string = String::from("\x1b[");

    let escapecode = match style {
        Style::Reset => String::from("0"),
        Style::Regular(ground) => String::from("0;") + &console_ground(ground),
        Style::Bright(ground) => String::from("1;") + &console_ground(ground),
    } + "m";
    string.push_str(&escapecode);
    string
}

fn console_ground(ground: FB) -> String {
    let style = match ground {
        FB::Background(color) => String::from("4") + console_color(color),
        FB::Foreground(color) => String::from("3") + console_color(color),
    };
    style
}

fn console_color(color: Color) -> &'static str {
    match color {
        Color::Black => "0",
        Color::Red => "1",
        Color::Green => "2",
        Color::Yellow => "3",
        Color::Blue => "4",
        Color::Magenta => "5",
        Color::Cyan => "6",
        Color::White => "7",
    }
}
