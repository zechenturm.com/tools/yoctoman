use crate::ErrorConverter;
use std::env;

macro_rules! req {
    ( $type:ident, $url:expr, $header:expr, $auth_token:expr) => {{
        let req = ureq::$type($url).set($header, $auth_token);
        req.call()
            .with_context("HTTP error")?
            .into_string()
            .with_context("failed to convert HTTP response to string")
    }};
}

enum AuthToken {
    Job(String),
    Private(String),
}

impl AuthToken {
    fn header(&self) -> &'static str {
        match self {
            Self::Job(_) => "JOB-TOKEN",
            Self::Private(_) => "PRIVATE-TOKEN",
        }
    }

    fn token(&self) -> &String {
        match self {
            Self::Job(t) => t,
            Self::Private(t) => t,
        }
    }
}

pub struct GitlabProject {
    path: String,
    url: String,
    auth_token: AuthToken,
}

impl GitlabProject {
    pub fn from_env() -> Result<GitlabProject, String> {
        let api_v4_url = env::var("CI_API_V4_URL")
            .or_else(|_| env::var("CI_SERVER_URL").map(|server| format!("{}/api/v4", server)))
            .with_context("Error getting Gitlab API URL from CI_API_V4_URL or CI_SERVER_URL")?;

        let project_path_slug = env::var("CI_PROJECT_PATH")
            .with_context("Error getting Gitlab project path slug from CI_PROJECT_PATH")?;
        let project_url_path = project_path_slug.replace('/', "%2F");

        let auth_token = std::env::var("CI_JOB_TOKEN")
            .map(AuthToken::Job)
            .or_else(|_| std::env::var("YOCTOMAN_PRIVATE_TOKEN").map(AuthToken::Private))
            .with_context("Error getting Gitlab auth/CI job token from CI_JOB_TOKEN or YOCTOMAN_PRIVATE_TOKEN")?;

        Ok(GitlabProject {
            url: format!("{}/projects/{}", api_v4_url, project_url_path),
            path: project_path_slug,
            auth_token,
        })
    }

    pub fn url(&self) -> String {
        self.url.clone()
    }

    pub fn get(&self, endpoint: &str) -> Result<String, String> {
        let url = format!("{}/{}", self.url(), endpoint);
        req!(get, &url, self.auth_token.header(), self.auth_token.token())
    }

    pub fn post(&self, endpoint: &str) -> Result<String, String> {
        let url = format!("{}/{}", self.url(), endpoint);
        req!(
            post,
            &url,
            self.auth_token.header(),
            self.auth_token.token()
        )
    }

    pub fn delete(&self, endpoint: &str) -> Result<(), String> {
        let url = format!("{}/{}", self.url(), endpoint);
        let req = ureq::delete(&url).set(self.auth_token.header(), self.auth_token.token());
        req.call().with_context("HTTP error")?;
        Ok(())
    }

    pub fn put(&self, endpoint: &str) -> ureq::Request {
        let url = format!("{}/{}", self.url(), endpoint);
        ureq::put(&url).set(self.auth_token.header(), self.auth_token.token())
    }

    pub fn path(&self) -> String {
        self.path.clone()
    }
}
