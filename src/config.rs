use std::{fmt::Write, path::Path};

use clap::{Args, Subcommand};
use log::debug;

use crate::{layers::Build, CliResult, ErrorConverter};

/// build configuration related commands
#[derive(Debug, Args)]
pub struct ConfigArgs {
    #[clap(subcommand)]
    command: ConfigCommand,
    #[clap(long, short = 'c', global = true, default_value = "layers.yaml")]
    config_path: String,
    #[clap(long, short = 'd', global = true, default_value = ".")]
    layer_dir: String,
}

#[derive(Debug, Subcommand)]
pub enum ConfigCommand {
    /// generate the correct BBLAYERS assignment that can be directly appended to `bblayers.conf`
    ///
    /// Will generate the assignment to BBLAYERS like so:
    ///
    ///     BBLAYERS = " \
    ///         /path/to/layer/meta-layer1 \
    ///         /path/to/layer/meta-laye2 \
    ///         ... \
    ///     "
    ///
    /// The output can be directly appended to `bblayers.conf` like this:
    ///
    ///     yoctoman config bblayers >> conf/bblayers.conf
    ///
    #[clap(verbatim_doc_comment)]
    Bblayers,
    Local,
    Target,
}

pub fn process(command: ConfigArgs) -> CliResult {
    let build_config = Build::read_from_disk(Path::new(&command.config_path))?;
    match command.command {
        ConfigCommand::Bblayers => process_bblayers(build_config, command.layer_dir),
        ConfigCommand::Local => process_local(build_config),
        ConfigCommand::Target => process_target(build_config),
    }
}

pub fn process_bblayers(build_config: Build, layer_dir: String) -> CliResult {
    let mut bblayers_conf = String::from("BBLAYERS = \" \\\n");
    for source in build_config.sources {
        let layer_path = Path::new(&layer_dir)
            .join(&source.name)
            .canonicalize()
            .with_context(format!("failed to canonicalize path for {}", source.name))?;
        if source.layers.is_empty() {
            writeln!(bblayers_conf, "\t{} \\", layer_path.display()).unwrap();
        } else {
            for layer in source.layers {
                let path = layer_path.join(layer);
                writeln!(bblayers_conf, "\t{} \\", path.display()).unwrap();
            }
        }
    }
    writeln!(bblayers_conf, "\"").unwrap();
    println!("{bblayers_conf}");
    Ok(())
}

pub fn process_local(build_config: Build) -> CliResult {
    debug!("local config: {:#?}", build_config.conf.local);
    for (key, value) in build_config.conf.local {
        println!("{key} = \"{value}\"")
    }
    Ok(())
}

pub fn process_target(build_config: Build) -> CliResult {
    println!("{}", build_config.target);
    Ok(())
}
