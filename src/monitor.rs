use std::{fs, thread::sleep, time::Duration};

use clap::{Args, Subcommand};
use termion::cursor;

use crate::{CliResult, ErrorConverter};

#[derive(Debug, Args)]
pub struct MonitorCommand {
    #[clap(subcommand)]
    command: Monitor,
    #[clap(short = 'i', global = true, default_value = "0")]
    interval: u64,
}

#[derive(Debug, Subcommand)]
pub enum Monitor {
    Pressure,
    CpuFreq,
}

pub fn process(cmd: MonitorCommand) -> CliResult {
    match cmd.command {
        Monitor::Pressure => print_with_interval(cmd.interval, print_pressure),
        Monitor::CpuFreq => print_with_interval(cmd.interval, print_cur_cpufreq),
    }
}

fn print_pressure() -> CliResult {
    print_file("CPU\n", "/proc/pressure/cpu")?;
    print_file("MEM\n", "/proc/pressure/memory")?;
    print_file("I/O\n", "/proc/pressure/io")
}

fn print_cur_cpufreq() -> CliResult {
    let cpus = num_cpus::get();
    // debug!("Number of CPUs reported: {}", cpus);
    for cpu in 0..cpus {
        print_file(
            "",
            &format!(
                "/sys/devices/system/cpu/cpu{}/cpufreq/scaling_cur_freq",
                cpu
            ),
        )?;
    }
    Ok(())
}

fn print_file(prefix: &str, filepath: &str) -> CliResult {
    let file = fs::read_to_string(filepath).with_context(format!("error reading {}", filepath))?;
    println!("{}{}", prefix, file.trim_end());
    Ok(())
}

fn print_with_interval<F: Fn() -> CliResult>(interval: u64, func: F) -> CliResult {
    print!("{}", cursor::Save);
    func()?;
    if interval > 0 {
        loop {
            print!("{}", cursor::Restore);
            func()?;
            sleep(Duration::from_secs(interval));
        }
    }
    Ok(())
}
