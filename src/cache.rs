use std::{
    fs::{self, File},
    io::{self, ErrorKind, Write},
    path::{Path, PathBuf},
};

use chrono::{DateTime, Duration, Utc};
use clap::{Args, Subcommand};
use glob::glob;
use log::{debug, info, trace};
use regex::Regex;
use sha2::{Digest, Sha256};

use crate::{CliResult, ErrorConverter};

/// artifact caching
///
/// cache build artifacts based on bitbake's task hashes
/// export the task hashes using `bitbake <target> -Snone`.
/// This will create a `locked-sigs.inc` file containing all the task hashes
#[derive(Debug, Subcommand)]
pub enum CacheCommand {
    Hash(HashCommandArgs),
    Check(CheckCommandArgs),
    Retrieve(ManipulationArgs),
    Deposit(ManipulationArgs),
    Clean(CleanCommandArgs),
}

#[derive(Debug, Args)]
pub struct HashCommandArgs {
    bb_sigfile_path: String,
    #[clap(long)]
    target: String,
}

#[derive(Debug, Args)]
pub struct CheckCommandArgs {
    bb_sigfile_path: String,
    cache_path: String,
    #[clap(long)]
    target: String,
}

#[derive(Debug, Args)]
pub struct ManipulationArgs {
    bb_sigfile_path: String,
    cache_path: String,
    artifacts: Vec<String>,
    #[clap(long)]
    target: String,
}

#[derive(Debug, Args)]
pub struct CleanCommandArgs {
    cache_path: String,
    #[clap(long)]
    days_older_than: i64,
}

#[derive(Debug)]
struct BitbakeJob {
    name: String,
    hash: String,
}

pub fn process(cmd: CacheCommand) -> CliResult {
    match cmd {
        CacheCommand::Hash(args) => {
            println!("{}", calculate_hash(args.bb_sigfile_path, args.target)?);
            Ok(())
        }
        CacheCommand::Check(args) => process_check(args),

        CacheCommand::Retrieve(args) => process_retrieve(args),
        CacheCommand::Deposit(args) => process_deposit(args),
        CacheCommand::Clean(args) => process_clean(args),
    }
}

fn calculate_hash(bb_sigfile_path: String, target: String) -> Result<String, String> {
    let path = fs::canonicalize(bb_sigfile_path)
        .with_context("failed to get absolute path to bb signature file")?;
    let lines = fs::read_to_string(path).with_context("failed to read signature file")?;

    let regex = Regex::new(&format!(r".*{}:(?P<job>.*):(?P<hash>[a-z0-9]+)", target)).unwrap();
    let mut hashes = Vec::<BitbakeJob>::new();

    for line in lines.lines() {
        if let Some(caps) = regex.captures(line) {
            let job = caps.name("job").unwrap().as_str();
            let hash = caps.name("hash").unwrap().as_str();
            hashes.push(BitbakeJob {
                name: job.to_string(),
                hash: hash.to_string(),
            });
        }
    }

    let mut hasher = Sha256::new();
    for job in &hashes {
        trace!("job {}: {}", job.name, job.hash);
        hasher.update(&job.hash);
    }
    let hash = format!("{:x}", hasher.finalize());
    debug!("found {} task hashes", hashes.len());
    if hashes.is_empty() {
        return Err("no Task Hashes found".into());
    }

    Ok(hash)
}

fn process_check(args: CheckCommandArgs) -> CliResult {
    let hash = calculate_hash(args.bb_sigfile_path, args.target)?;
    debug!("caclulated hash: {}", hash);

    let cache_path = get_or_create_cache_base_path(args.cache_path)?.join(&hash);
    if cache_path.exists() {
        debug!("hash {} found in cache", hash);
        if let Some(timestamp) = read_timestamp(&cache_path) {
            debug!("last read: {}", timestamp);
        } else {
            debug!("cache has never been read");
        }
        Ok(())
    } else {
        debug!("hash {} not found in cache", hash);
        Err(1.into())
    }
}

fn process_retrieve(args: ManipulationArgs) -> CliResult {
    let cache_base_path = get_or_create_cache_base_path(args.cache_path)?;
    let hash = calculate_hash(args.bb_sigfile_path, args.target)?;
    let cache_path = cache_base_path.join(&hash);
    if !cache_path.exists() {
        info!(
            "hash {} not found in cache, return non zero exit code",
            hash
        );
        return Err(1.into());
    }
    for artifact_glob in args.artifacts {
        process_retrieve_artifact_glob(&cache_path, artifact_glob)?;
    }
    update_or_create_timestamp(cache_path.as_path())?;

    Ok(())
}

fn process_deposit(args: ManipulationArgs) -> CliResult {
    let cache_base_path = get_or_create_cache_base_path(args.cache_path)?;
    let hash = calculate_hash(args.bb_sigfile_path, args.target)?;
    let cache_path = cache_base_path.join(&hash);
    if !cache_path.exists() {
        debug!("hash {} does not exist yet, creating", hash);
        fs::create_dir_all(&cache_path).with_context("failed to create cache dir")?;
    } else {
        debug!(
            "hash {} already exists, overwriting existing artifacts",
            hash
        );
    }
    for artifact in args.artifacts {
        copy_glob(&artifact, None, &cache_path)?;
    }
    update_or_create_timestamp(&cache_path)?;

    Ok(())
}

fn process_clean(args: CleanCommandArgs) -> CliResult {
    let now = Utc::now();
    let duration = Duration::days(args.days_older_than);
    let threshold_timestamp = now - duration;
    let caches = fs::read_dir(args.cache_path).with_context("failed to read cache dir")?;

    for cache in caches {
        let cache = cache.with_context("failed to read cache")?;
        debug!("checking cache {}", cache.path().display());
        if needs_cleaning(&cache.path(), threshold_timestamp) {
            debug!("cleaning cache {}", cache.path().display());
            fs::remove_dir_all(&cache.path())
                .with_context(format!("failed to remove cache {}", cache.path().display()))?;
        }
    }
    Ok(())
}

fn get_or_create_cache_base_path(cache_path: String) -> Result<PathBuf, String> {
    match fs::canonicalize(&cache_path) {
        Ok(path) => Ok(path),
        Err(e) if e.kind() == ErrorKind::NotFound => {
            debug!(
                "cache directory \"{}\" does not exist, creating it",
                cache_path
            );
            fs::create_dir_all(&cache_path)
                .with_context(format!("Failed to create cache dir at {}", cache_path))?;
            fs::canonicalize(&cache_path).with_context("failed to canonicalize cache path")
        }
        Err(e) => Err(format!("{}", e)),
    }
}

fn process_retrieve_artifact_glob(cache_path: &Path, artifact_glob: String) -> CliResult {
    let artifact_cache_glob = cache_path.join(artifact_glob);
    let artifact_cache_glob = artifact_cache_glob.to_string_lossy();
    trace!(
        "glob to find artifacts (including cache base path): {}",
        artifact_cache_glob
    );
    copy_glob(&artifact_cache_glob, Some(cache_path), Path::new("."))?;
    Ok(())
}

fn create_ancestors(path: &Path) -> io::Result<()> {
    if let Some(parent) = path.parent() {
        fs::create_dir_all(parent)
    } else {
        Ok(())
    }
}

fn copy_glob(pattern: &str, from_prefix: Option<&Path>, to_prefix: &Path) -> CliResult {
    for artifact in glob(pattern)
        .with_context("failed to read glob pattern")?
        .filter_map(Result::ok)
        .filter(|a| a.is_file())
    {
        debug!("found path: {}", artifact.display());
        let relative_path = from_prefix.map_or(Ok(artifact.as_path()), |prefix| {
            artifact
                .strip_prefix(prefix)
                .with_context("failed to strip from_prefix")
        })?;
        let to_path = to_prefix.join(relative_path);
        create_ancestors(&to_path).with_context(format!(
            "failed to create parent directories for {}",
            to_path.display()
        ))?;
        copy(&artifact, &to_path)?;
    }
    Ok(())
}

fn copy(from: &Path, to: &Path) -> CliResult {
    debug!("copying {} to {}", from.display(), to.display());
    create_ancestors(to)?;
    match fs::copy(from, to).with_context(format!("failed to copy {}", from.display())) {
        Ok(_) => Ok(()),
        Err(e) => {
            fs::remove_file(to).with_context(format!(
                "failed to copy {} and remove it after copying failed",
                from.display()
            ))?;
            Err(e.into())
        }
    }
}

fn update_or_create_timestamp(cache_path: &Path) -> CliResult {
    log::debug!("writing timestamp");
    let timestamp = Utc::now().to_rfc3339();
    let mut file_path = cache_path.to_path_buf();
    file_path.push("timestamp");
    let mut file = File::create(&file_path).with_context("failed to create timestamp file")?;
    write!(file, "{}", timestamp).with_context("failed to write timestamp to file")?;
    Ok(())
}

fn read_timestamp(cache_path: &Path) -> Option<DateTime<Utc>> {
    let mut file_path = cache_path.to_path_buf();
    file_path.push("timestamp");
    let timestamp = fs::read_to_string(file_path).ok()?;
    let timestamp = DateTime::parse_from_rfc3339(&timestamp).ok()?;
    Some(timestamp.into())
}

fn needs_cleaning(cache_path: &Path, threshold: DateTime<Utc>) -> bool {
    if let Some(cache_timestamp) = read_timestamp(cache_path) {
        cache_timestamp < threshold
    } else {
        true
    }
}
