use std::io::{self, Read, Write};
use std::path::Path;
use std::{fs, time::Duration};

use crate::{CliResult, ErrorConverter};

use bollard::container::{AttachContainerOptions, AttachContainerResults};
use futures_util::StreamExt;
use tokio::{io::AsyncWriteExt, time::sleep};

use bollard::{
    container::{self, RemoveContainerOptions},
    models::HostConfig,
    Docker,
};
use termion::raw::IntoRawMode;
use tokio::task::spawn;

// const IMAGE: &str = "registry.gitlab.ost.ch:45023/tech/inf/public/docker-images/yocto-build";
const IMAGE: &str = "yocto-build";

pub async fn process() -> CliResult {
    let docker =
        Docker::connect_with_local_defaults().with_context("failed to connect to docker")?;

    let container = create_container(&docker, IMAGE.into()).await?;

    let AttachContainerResults {
        mut output,
        mut input,
    } = attach_to_container(&docker, &container).await?;

    spawn(async move {
        let mut stdin = termion::async_stdin().bytes();

        loop {
            if let Some(Ok(b)) = stdin.next() {
                input.write(&[b]).await.ok();
            } else {
                sleep(Duration::from_nanos(10)).await;
            }
        }
    });

    let stdout = io::stdout();
    let mut stdout = stdout
        .lock()
        .into_raw_mode()
        .with_context("failed to open stdout")?;

    while let Some(Ok(out)) = output.next().await {
        stdout
            .write_all(out.into_bytes().as_ref())
            .with_context("failed to copy bytes to stdout")?;
        stdout.flush().with_context("failed to flush stdout")?;
    }

    remove_container(&docker, &container).await?;

    Ok(())
}

async fn create_container(docker: &Docker, image: String) -> Result<String, String> {
    let host_config = HostConfig {
        binds: Some(vec![format_bindmount("~/.config", "/home/ubuntu/.config")?]),
        ..Default::default()
    };
    let config = container::Config {
        image: Some(image.as_str()),
        tty: Some(true),
        attach_stdin: Some(true),
        attach_stdout: Some(true),
        attach_stderr: Some(true),
        open_stdin: Some(true),
        host_config: Some(host_config),
        ..Default::default()
    };

    let container = docker
        .create_container::<&str, &str>(None, config)
        .await
        .with_context("failed to create container")?;
    docker
        .start_container::<String>(&container.id, None)
        .await
        .with_context("failed to start container")?;
    Ok(container.id)
}

async fn attach_to_container(
    docker: &Docker,
    container: &str,
) -> Result<AttachContainerResults, String> {
    let attach_ops = AttachContainerOptions::<String> {
        stdin: Some(true),
        stdout: Some(true),
        stderr: Some(true),
        stream: Some(true),
        ..Default::default()
    };
    docker
        .attach_container(container, Some(attach_ops))
        .await
        .map_err(|e| format!("failed to attach to container: {}", e))
}

async fn remove_container(docker: &Docker, container: &str) -> CliResult {
    let remove_opts = RemoveContainerOptions {
        force: true,
        ..Default::default()
    };
    docker
        .remove_container(container, Some(remove_opts))
        .await
        .with_context("failed to remove container")?;
    Ok(())
}

fn format_bindmount(host_path: &str, image_path: &str) -> Result<String, String> {
    let host_path = if host_path.starts_with('~') {
        let home_dir = home::home_dir().ok_or(format!(
            "bindmount {} is relative to home directory, but failed to get home directory",
            host_path
        ))?;
        home_dir.join(host_path.trim_start_matches("~/"))
    } else {
        Path::new(host_path).to_path_buf()
    };
    let dir = fs::canonicalize(host_path)
        .map_err(|e| format!("failed to get absolute host path: {}", e))?;

    Ok(format!("{}:{}", dir.display(), image_path))
}
