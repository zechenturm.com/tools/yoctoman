use std::fmt::Display;

use clap::{Args, Subcommand};

use ::serde::Deserialize;
use ::serde_json;
use log::info;

use chrono::{DateTime, Duration, Local};

use crate::{CliResult, ErrorConverter};

use super::console_style::{set_console, Color, Style, FB};
use super::gitlab::GitlabProject;

#[derive(Debug, Subcommand)]
#[clap(about = "manage gitlab CI pipelines")]
pub enum PipelineCommand {
    Info,
    New(NewPipelineArgs),
    Delete(DeletePipelineArgs),
}

#[derive(Args, Debug)]
pub struct NewPipelineArgs {
    referene: String,
}

#[derive(Args, Debug)]
pub struct DeletePipelineArgs {
    ids: Vec<usize>,
    #[clap(long)]
    all_but_newest_n: Option<usize>,
}

pub fn process_pipeline_command(cmd: PipelineCommand) -> CliResult {
    match cmd {
        PipelineCommand::Info => pipeline_info_for_project(),
        PipelineCommand::New(args) => spawn_pipeline_for_project(args),
        PipelineCommand::Delete(args) => delete_pipeline_for_project(args),
    }
}

fn pipeline_info_for_project() -> CliResult {
    for pipeline in request_pipelines()? {
        println!("{:#}", pipeline);
    }
    Ok(())
}

fn spawn_pipeline_for_project(args: NewPipelineArgs) -> CliResult {
    let project = GitlabProject::from_env()?;
    let res = project.post(&format!("pipeline?ref={}", args.referene))?;
    let data: GitlabPipeline =
        serde_json::from_str(&res).with_context("failed to parse JSON response from server")?;
    info!("Created {:#}", data);
    Ok(())
}

fn delete_pipeline_for_project(args: DeletePipelineArgs) -> CliResult {
    let project = GitlabProject::from_env()?;

    let delete_pipeline = |id| project.delete(&format!("pipelines/{}", id));

    for id in args.ids {
        delete_pipeline(id)?;
    }

    if let Some(n) = args.all_but_newest_n {
        let mut pipelines = request_pipelines()?;
        let number_of_pipelines_to_delete = pipelines.len() - n;
        pipelines.sort_by(|a, b| a.created_at.cmp(&b.created_at));
        for pipeline in pipelines.iter().take(number_of_pipelines_to_delete) {
            info!("deleting {:#}", pipeline);
            delete_pipeline(pipeline.id)?;
        }
    }
    Ok(())
}

#[derive(Deserialize)]
struct GitlabPipeline {
    id: usize,
    status: String,
    #[serde(rename(deserialize = "ref"))]
    reference: String,
    created_at: DateTime<Local>,
    updated_at: DateTime<Local>,
}

impl GitlabPipeline {
    fn status_with_color(&self) -> String {
        let color = match &*self.status {
            "failed" => Color::Red,
            "success" => Color::Green,
            "canceled" => Color::Yellow,
            "running" => Color::Cyan,
            _ => Color::White,
        };
        format!(
            "{}{:>8}{}",
            set_console(Style::Bright(FB::Foreground(color))),
            self.status,
            set_console(Style::Reset)
        )
    }

    fn ref_with_color(&self) -> String {
        format!(
            "{}{}{}",
            set_console(Style::Bright(FB::Foreground(Color::White))),
            self.reference,
            set_console(Style::Reset)
        )
    }

    fn format_time(timestamp: &DateTime<Local>) -> String {
        timestamp.format("%_H:%M %-d.%-m.%y").to_string()
    }

    fn format_duration(duration: &Duration) -> String {
        format!(
            "{:02}:{:02}",
            duration.num_hours(),
            duration.num_minutes() - duration.num_hours() * 60
        )
    }

    fn time_difference(&self) -> Duration {
        self.updated_at - self.created_at
    }
}

macro_rules! ternary {
    ($condition:expr, $true_value:expr, $false_value:expr) => {{
        if ($condition) {
            $true_value
        } else {
            $false_value
        }
    }};
}

impl Display for GitlabPipeline {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Pipline ID {} for {} {} at {} (ran for {})",
            self.id,
            ternary!(f.alternate(), self.ref_with_color(), self.reference.clone()),
            ternary!(f.alternate(), self.status_with_color(), self.status.clone()),
            GitlabPipeline::format_time(&self.updated_at),
            GitlabPipeline::format_duration(&self.time_difference())
        )
    }
}

fn request_pipelines() -> Result<Vec<GitlabPipeline>, String> {
    let project = GitlabProject::from_env()?;
    let res = project.get("pipelines?order_by=updated_at")?;
    let data: Vec<GitlabPipeline> =
        serde_json::from_str(&res).with_context("failed to parse JSON response from server")?;
    Ok(data)
}
