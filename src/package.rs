use std::{
    cmp::Ordering,
    collections::HashMap,
    fmt::{self, Display},
    fs::{self, File},
    marker::PhantomData,
    str::FromStr,
};

use chrono::{DateTime, Local};
use clap::{ArgGroup, Args, Subcommand};
use log::{debug, info};
use regex::{Captures, Regex};
use serde::{
    de::{self, Visitor},
    Deserializer,
};

use crate::{
    console_style::{set_console, Color, Style, FB},
    gitlab::GitlabProject,
    CliResult, ErrorConverter,
};

#[derive(Debug, Subcommand)]
#[clap(about = "manage gitlab packages")]
pub enum PackageCommand {
    Info(InfoArgs),
    Files(FileArgs),
    Delete(DeleteArgs),
    Upload(UploadArgs),
}

#[derive(Debug, Args)]
pub struct FileArgs {
    #[clap(long, required = true, conflicts_with_all= &["name", "version"], required_unless_present_any = &["name", "version"])]
    id: Option<usize>,
    #[clap(long, conflicts_with = "id")]
    name: Option<String>,
    #[clap(long, conflicts_with = "id")]
    version: Option<DateVersion>,
}

#[derive(Debug, Args)]
pub struct InfoArgs {
    #[clap(long)]
    name: Option<String>,
    #[clap(long)]
    newest: Option<usize>,
}

#[derive(Debug, Args)]
#[clap(group(ArgGroup::new("todelete").required(true).args(&["id", "all_but_newest_n"])))]
pub struct DeleteArgs {
    #[clap(long)]
    dry_run: bool,
    #[clap(long)]
    id: Option<usize>,
    #[clap(long, conflicts_with("id"))]
    all_but_newest_n: Option<usize>,
}

#[derive(Debug, Args)]
pub struct UploadArgs {
    #[clap(long)]
    version: DateVersion,
    #[clap(long)]
    name: String,
    files: Vec<String>,
}

type PackageMap = HashMap<String, Vec<GitlabPackage>>;

#[derive(Debug, Clone, Eq)]
struct DateVersion {
    year: usize,
    month: usize,
    day: usize,
}

impl Ord for DateVersion {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.year.cmp(&other.year) {
            Ordering::Greater => Ordering::Greater,
            Ordering::Less => Ordering::Less,
            Ordering::Equal => match self.month.cmp(&other.month) {
                Ordering::Greater => Ordering::Greater,
                Ordering::Less => Ordering::Less,
                Ordering::Equal => match self.day.cmp(&other.day) {
                    Ordering::Greater => Ordering::Greater,
                    Ordering::Less => Ordering::Less,
                    Ordering::Equal => Ordering::Equal,
                },
            },
        }
    }
}

impl PartialOrd for DateVersion {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for DateVersion {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}

impl Display for DateVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{:02}.{:02}.{:02}", self.year, self.month, self.day)
    }
}

impl FromStr for DateVersion {
    type Err = String;
    fn from_str(string: &str) -> Result<Self, Self::Err> {
        static REGEX: &str = r"^(?P<year>\d{2}).(?P<month>\d{2}).(?P<day>\d{2})$";
        let r = Regex::new(REGEX).unwrap();
        let captures = r
            .captures(string)
            .ok_or_else(|| format!("\"{string}\" does not match version regex '{REGEX}'"))?;

        let parse_field = |string: &Captures, name: &'static str| -> Result<usize, String> {
            let string = string
                .name(name)
                .ok_or_else(|| format!("no match found for {name}"))?
                .as_str();
            let version: usize = string
                .parse()
                .with_context("error parsing version number")?;
            Ok(version)
        };

        let year = parse_field(&captures, "year")?;
        let month = parse_field(&captures, "month")?;
        let day = parse_field(&captures, "day")?;

        Ok(DateVersion { year, month, day })
    }
}

#[derive(Debug, serde::Deserialize)]
struct GitlabPackage {
    id: usize,
    name: String,
    #[serde(deserialize_with = "deserialize_dateversion")]
    version: DateVersion,
    package_type: String,
    status: String,
    created_at: DateTime<Local>,
}

impl Display for GitlabPackage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{} package {} (id: {}) version {} created at {} with status {}",
            self.package_type,
            self.name,
            self.id,
            self.version,
            self.created_at.format("%_H:%M %-d.%-m.%y"),
            self.status
        )
    }
}

fn deserialize_dateversion<'de, D>(deserializer: D) -> Result<DateVersion, D::Error>
where
    D: Deserializer<'de>,
{
    struct StringVisitor(PhantomData<fn() -> DateVersion>);

    impl<'de> Visitor<'de> for StringVisitor {
        type Value = DateVersion;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("string")
        }

        fn visit_str<E>(self, value: &str) -> Result<DateVersion, E>
        where
            E: de::Error,
        {
            FromStr::from_str(value).map_err(|e| E::custom(e))
        }
    }
    deserializer.deserialize_any(StringVisitor(PhantomData))
}

#[derive(Debug, serde::Deserialize)]

struct PackageFile {
    id: usize,
    package_id: usize,
    #[serde(rename(deserialize = "file_name"))]
    name: String,
    size: usize,
    created_at: DateTime<Local>,
}

impl Display for PackageFile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{} (id: {}, package id {}), size {} bytes, created at {}",
            self.name,
            self.id,
            self.package_id,
            self.size,
            self.created_at.format("%_H:%M %-d.%-m.%y"),
        )
    }
}

pub fn execute(command: PackageCommand) -> CliResult {
    match command {
        PackageCommand::Info(args) => package_info(args),
        PackageCommand::Files(args) => list_packages_files(args),
        PackageCommand::Delete(args) => delete_packages(args),
        PackageCommand::Upload(args) => upload_package(args),
    }
}

fn optional_compare<T: PartialEq>(lhs: &T, rhs: &Option<T>) -> bool {
    rhs.is_none() || lhs == rhs.as_ref().unwrap()
}

fn find_packages_by_name_or_version(
    project: &GitlabProject,
    name: Option<String>,
    version: Option<DateVersion>,
) -> Result<Vec<GitlabPackage>, String> {
    let resp = project.get("packages")?;
    let packages: Vec<GitlabPackage> =
        serde_json::from_str(&resp).with_context("unable to parse JSON from server response")?;

    Ok(packages
        .into_iter()
        .filter(|p| optional_compare(&p.name, &name) && optional_compare(&p.version, &version))
        .collect())
}

fn group_packages(packages: Vec<GitlabPackage>) -> PackageMap {
    let mut grouped_packages: HashMap<String, Vec<GitlabPackage>> = HashMap::new();
    for package in packages {
        let maybe_versions = grouped_packages.get_mut(&package.name);
        if let Some(versions) = maybe_versions {
            versions.push(package);
        } else {
            grouped_packages.insert(package.name.clone(), vec![package]);
        }
    }

    for versions in &mut grouped_packages.values_mut() {
        versions.sort_by(|a, b| b.version.cmp(&a.version))
    }
    grouped_packages
}

fn get_packages_grouped(filter_name: Option<String>) -> Result<PackageMap, String> {
    let project = GitlabProject::from_env()?;
    let mut endpoint = String::from("packages?order_by=version&sort=asc");
    if let Some(name) = filter_name {
        endpoint = format!("{}&package_name={}", endpoint, name);
    };
    let response = project.get(&endpoint)?;
    let packages: Vec<GitlabPackage> = serde_json::from_str(&response)
        .with_context("unable to parse JSON from server response")?;
    Ok(group_packages(packages))
}

fn package_info(args: InfoArgs) -> CliResult {
    let grouped_packages = get_packages_grouped(args.name)?;

    for (name, packages) in grouped_packages {
        println!("{}", name);
        let packages_to_show = if let Some(n) = args.newest {
            packages.into_iter().take(n).collect()
        } else {
            packages
        };
        for package in packages_to_show {
            println!("\t{}", package)
        }
    }
    Ok(())
}

fn list_packages_files(args: FileArgs) -> CliResult {
    let project = GitlabProject::from_env()?;
    let print_files_info = |id: usize, prefix: &str| -> CliResult {
        let response = project.get(&format!("packages/{}/package_files", id))?;
        let files: Vec<PackageFile> = serde_json::from_str(&response)
            .with_context("unable to parse JSON from server response")?;
        for file in files {
            println!("{}{}", prefix, file);
        }
        Ok(())
    };
    if let Some(id) = args.id {
        print_files_info(id, "")?;
        return Ok(());
    }

    for package in find_packages_by_name_or_version(&project, args.name, args.version)? {
        println!(
            "{}{} - {}{}:",
            set_console(Style::Bright(FB::Foreground(Color::White))),
            package.name,
            package.version,
            set_console(Style::Reset)
        );
        print_files_info(package.id, "\t")?;
    }
    Ok(())
}

fn delete_packages(args: DeleteArgs) -> CliResult {
    let not_a_dry_run = !args.dry_run;
    let project = GitlabProject::from_env()?;
    let delete_package = |id: usize| -> CliResult {
        info!("deleting package {} from project {}", id, project.path());
        if not_a_dry_run {
            project.delete(&format!("packages/{}", id))?;
        }
        Ok(())
    };

    if let Some(id) = args.id {
        delete_package(id)?;
        return Ok(());
    }

    if let Some(newest_n) = args.all_but_newest_n {
        let mut packages = get_packages_grouped(None)?;
        for versions in packages.values_mut() {
            let to_delete = versions.iter().skip(newest_n);
            for package in to_delete {
                delete_package(package.id)?;
            }
        }
    }
    Ok(())
}

fn upload_package(args: UploadArgs) -> CliResult {
    let project = GitlabProject::from_env()?;
    let upload_package_endpoint = format!("packages/generic/{}/{}", args.name, args.version);
    for file_name in args.files {
        let file_path = fs::canonicalize(&file_name)
            .with_context(format!("unable to crate file path for {}", file_name))?;
        let file =
            File::open(&file_path).with_context(format!("failed to open file {}", file_name))?;
        let actual_file_name = file_name.split('/').last().unwrap();
        let req = project
            .put(&format!("{}/{}", upload_package_endpoint, actual_file_name))
            .set("Content-Type", "application/json");
        debug!("URL: {}", req.url());
        let response = req
            .send(file)
            .with_context(format!(
                "failed to upload file {} to package {} v{}",
                file_name, args.name, args.version
            ))?
            .into_string()
            .unwrap_or_else(|e| format!("failed to turn response into string: {e:?}"));
        debug!("got respone: {response}");
    }
    Ok(())
}
